$(document).ready(function(){
    $("#productType").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".form").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".form").hide();
            }
        });
    }).change();
});