$(document).ready(function () {

    $('#mass-delete-btn').on('click', function () {
        let ids = $(':checkbox:checked').map(function () {
                return this.value;
            }).get();
            if (ids.length === 0) {
                alert('Please select at least one checkbox to delete');
            } else {
                $.ajax({
                    url: 'massDelete.php',
                    method: 'POST',
                    data: { ids: ids.join(",") },
                    success: function () {
                        ids.forEach(id => {
                            $(`#card-${id}`).fadeOut(500);
                            $(`#card-${id}`).remove();
                            $(`#card-${id} .delete-checkbox`).remove();
                        });
                    }
                });
            }
    });

});