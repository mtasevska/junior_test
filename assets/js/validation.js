$(document).ready(function () {

    $("#product_form").validate({
        // Specify validation rules
        rules: {
            sku: "required",
            name: "required",
            price: {
                required: true,
                number: true
            },
            size: {
                required: true,
                number: true
            },
            height: {
                required: true,
                number: true
            },
            width: {
                required: true,
                number: true
            },
            length: {
                required: true,
                number: true
            },
            weight: {
                required: true,
                number: true
            },
        },
        errorPlacement: function (error, label) {
            error.insertBefore(label);
            error.css('color', 'red');
            error.css('display', 'block');
        },

        messages: {
            sku: "Please, submit required data",
            name: "Please, submit required data",
            price: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
            },
            size: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
            },
            height: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
            },
            width: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
            },
            length: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
            },
            weight: {
                required: "Please, submit required data",
                number: "Please, provide the data of indicated type",
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});