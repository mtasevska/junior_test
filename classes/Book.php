<?php

namespace Product;
use Product\Product;
use Product\ProductAttribute;
use PDO;
require_once './classes/Product.php';
require_once './classes/ProductAttribute.php';

class Book extends Product {

    public $weight;

    public function __construct($data) {
        
        $this->weight = $data['weight'];

        parent::__construct($data);
    }

    public static function setAttributes($dataAttributes) {
        DB::connect();
        $sql = "SELECT id FROM products ORDER BY id DESC";
        $stmt   = DB::$pdo->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetch();
        $product_id = $result['id'];
        $weight = $dataAttributes['weight'];

        $sql = "SELECT id from attributes WHERE attribute_name = 'weight'";
        $stmt = DB::$pdo->prepare($sql);
        $stmt->execute();

        $atr_id = $stmt->fetch();
        $attribute_id = $atr_id['id'];

        ProductAttribute::insertAttributes($product_id, $attribute_id, $weight);
    }
}

?>
