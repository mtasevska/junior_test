<?php

namespace Product;
use mysqli;
use \PDO;
use PDOException;
use Product\ProductTypes;
use Product\Dvd;
use Product\Furniture;
use Product\Book;

require_once './classes/ProductTypes.php';
require_once './classes/Dvd.php';
require_once './classes/Furniture.php';
require_once './classes/Book.php';
require_once './classes/Product.php';


class DB
{
    public static $pdo = null;

    public static function connect()
    {
        if (is_null(self::$pdo)) {
            try {
                self::$pdo = new \PDO("mysql:dbname=" . DBNAME . ";host=" . HOST, MYACCOUNT, MYPASSWORD);
            } catch (PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }
    }
}
