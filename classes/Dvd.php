<?php

namespace Product;
use Product\Product;

require_once './classes/Product.php';
require_once './classes/ProductAttribute.php';

class Dvd extends Product {

    public $size;

    public function __construct($data) {

        $this->size = $data['size'];

        parent::__construct($data);
    }

    public static function setAttributes($dataAttributes) {
        DB::connect();
        $sql = "SELECT id FROM products ORDER BY id DESC";
        $stmt   = DB::$pdo->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetch();
        $product_id = $result['id'];
        $size = $dataAttributes['size'];

        $sql = "SELECT id from attributes WHERE attribute_name = 'size'";
        $stmt = DB::$pdo->prepare($sql);
        $stmt->execute();

        $atr_id = $stmt->fetch();
        $attribute_id = $atr_id['id'];

        ProductAttribute::insertAttributes($product_id, $attribute_id, $size);
    }
}

?>
