<?php

namespace Product;
use Product\Product;

require_once './classes/Product.php';
require_once './classes/ProductAttribute.php';



class Furniture extends Product {

    public $height;
    public $width;
    public $length;

    public function __construct($data) {
        
        $this->height = $data['height'];
        $this->width = $data['width'];
        $this->length = $data['length'];

        parent::__construct($data);
    }

    public static function insertAttributes($dataAttributes) {
        DB::connect();

        echo 'Furniture here';
        var_dump($dataAttributes);
        die();
    }

    public static function getAttributes($dataAttributes) {
        DB::connect();
        $sql = "SELECT id FROM products ORDER BY id DESC";
        $stmt   = DB::$pdo->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetch();
        $product_id = $result['id'];
        $height = $dataAttributes['height'];
        $width = $dataAttributes['width'];
        $length = $dataAttributes['length'];

        $sql = "SELECT id from attributes WHERE attribute_name = 'height'";
        $stmt = DB::$pdo->prepare($sql);
        $stmt->execute();
        $atr_id = $stmt->fetch();
        $height_id = $atr_id['id'];

        $sql = "SELECT id from attributes WHERE attribute_name = 'width'";
        $stmt = DB::$pdo->prepare($sql);
        $stmt->execute();
        $atr_id = $stmt->fetch();
        $width_id = $atr_id['id'];

        $sql = "SELECT id from attributes WHERE attribute_name = 'length'";
        $stmt = DB::$pdo->prepare($sql);
        $stmt->execute();
        $atr_id = $stmt->fetch();
        $length_id = $atr_id['id'];
  

        ProductAttribute::insertAttributes($product_id, $height_id, $height);
        ProductAttribute::insertAttributes($product_id, $width_id, $width);
        ProductAttribute::insertAttributes($product_id, $length_id, $length);

    }
}

?>