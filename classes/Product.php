<?php

namespace Product;
use Product\Interfaces\ProductInterface;
use Product\DB;
use \PDO;

require_once './classes/DB.php';

class Product {
    public $sku;
    public $name;
    public $price;
    public $product_type_id;
    private static $table = 'products';

    public function __construct($data) 
    {
        $this->sku = $data['sku'];
        $this->name = $data['name'];
        $this->price = $data['price'];
        $this->product_type_id = $data['product_type_id'];
    }

    public function getColumns() {
        return [
            'sku',
            'name',
            'price',
            'product_type_id'
        ];
    }

    public function getData() {
        return [
            $this->sku,
            $this->name,
            $this->price,
            $this->product_type_id
        ];
    }

    public static function getTable() {
        return self::$table;
    }
    
    public static function insert($object) {
        
        DB::connect();

        $table = $object->getTable();
        $columns = $object->getColumns();
        $table_columns = implode(',', $columns, );
        $placeholders = [];
        foreach($columns as $column) {
            array_push($placeholders, "?");
        }
        $table_placeholders = implode(',', $placeholders);
        
        $sql = "INSERT INTO $table($table_columns) VALUES($table_placeholders)";

        $stmt = DB::$pdo->prepare($sql);

        if($stmt->execute($object->getData())) {
            return true;
        } else {
            return false;
        }

    }

    public static function getAll() {

        DB::connect();

        $sql = "SELECT * FROM products WHERE 1";

        $stmt = DB::$pdo->query($sql);
        
        return  $stmt->fetchAll();

    }

}


?>