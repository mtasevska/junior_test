<?php

namespace Product;
include 'constants.php';


class ProductAttribute {
    public $product_id;
    public $attribute_id;
    public $value;
    private static $table = 'product_attribute';


    public function __construct($product_id, $attribute_id, $value)
    {   
        $this->product_id = $product_id;
        $this->attribute_id = $attribute_id;
        $this->value = $value;
        
    }

    public function getColumns() {
        return [
            'product_id',
            'attribute_id',
            'value',
        ];
    }

    public function getData() {
        return [
            $this->product_id,
            $this->attribute_id,
            $this->value,
        ];
    }

    public static function getTable() {
        return self::$table;
    }
    

    public static function insertAttributes($product_id, $attribute_id, $value) {

        DB::connect();
        
        $sql = "INSERT INTO product_attribute(`product_id`, `attribute_id`, `value`) VALUES($product_id, $attribute_id, $value)";

        $stmt = DB::$pdo->prepare($sql);

        if($stmt->execute()) {
            header("Location:productList.php");
        } else {
            return false;
        }
    }

    public static function getAll($table_name) {

        DB::connect();

        $sql = "SELECT * FROM $table_name WHERE 1";

        $stmt = DB::$pdo->query($sql);

        return  $stmt->fetchAll();

    }
}