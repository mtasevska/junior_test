<?php

namespace Product;

use Product\DB;
use PDO;

require_once './classes/DB.php';

class ProductTypes {

    public $type_name;

    public function __construct($type_name)
    {
        $this->type_name = $type_name;
    }

    public static function getTypeName($id) {
        DB::connect();
        $sql = "SELECT type_name FROM product_types where id = $id";
        $stmt = DB::$pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_OBJ);

        $name = $result->type_name;
        return $name;
    }
}
