CREATE TABLE `product_types` (
    `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `type_name` VARCHAR(32)
    
    );
    
CREATE TABLE `products` (
    `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `sku` VARCHAR(32),
    `name` VARCHAR(64),
    `price` INT UNSIGNED,
    `product_type_id` INT UNSIGNED,
    
     CONSTRAINT from_product_types_to_products FOREIGN KEY(product_type_id) REFERENCES product_types(id)
    );
    
CREATE TABLE `attributes` (
    `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`attribute_name` VARCHAR(32),
	`attribute_metric` VARCHAR(32)
     );
     
CREATE TABLE `product_attribute` (
    `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`product_id` INT UNSIGNED,
	`attribute_id` INT UNSIGNED,
	`value` VARCHAR(32),
     INDEX product_ind (product_id),  
     INDEX attribute_ind (attribute_id),  

    
     CONSTRAINT from_products_to_product_attribute FOREIGN KEY(product_id) REFERENCES products(id),
     CONSTRAINT from_attributes_to_product_attribute FOREIGN KEY(attribute_id) REFERENCES attributes(id)
    
	);
    
INSERT INTO product_types(type_name) VALUES  
('dvd'),  
('furniture'),  
('book');

INSERT INTO attributes(attribute_name, attribute_metric) VALUES
('Size', 'MB'),
('Height', 'cm'),
('Width', 'cm'),
('Length', 'cm'),
('Weight', 'Kg');