<?php

use Product\Product;
use Product\ProductAttribute;

require_once './classes/Product.php';
require_once './classes/ProductAttribute.php';
?>

<!doctype html>
<html lang="en">

<head>
    <title>Product List</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body>
    <section id="header" class="mt-3">
        <div class="container">
            <div class="row d-flex">
                <div class="col">
                    <h3>Product List</h3>
                </div>

                <div class="col offset-5">
                    <button type="button" class="btn-small btn-secondary"><a href="productAdd.php" class="text-decoration-none text-white">ADD</a></button>
                    <button type="button" class="btn-small btn-danger" id="mass-delete-btn">MASS DELETE</button>
                </div>
            </div>
            <hr class="mt-2 mb-3" />
        </div>
    </section>

    <section id="main">
        <div class="container">
            <div class="row row-cols-3">
                        <?php
                            $products = Product::getAll();
                        ?>
                        <?php if (empty($products)) : ?>
                            <p class='text-center'>No products in database added</p>
                        <?php endif; ?>

                        <?php foreach ($products as $product) : ?>
                            <?php $attributes = ProductAttribute::getAttributes($product['id']); ?>
                            <div id="card-<?= $product['id'] ?>" class='col'>
                                <div class='card mb-3' style='min-width: 18rem;min-height:20rem'>
                                    <div class='card-header'>
                                        <div class='form-check'>
                                            <input class="form-check-input delete-checkbox" id="card-<?= $product['id'] ?>" type='checkbox' value="<?= $product['id']; ?>">
                                        </div>
                                    </div>
                                    <div class='card-body text-center'>
                                        <p class='card-text'><?= $product['sku']; ?></p>
                                        <p class='card-text'><?= $product['name']; ?></p>
                                        <p class='card-text'><?= $product['price'] . ' $'; ?></p>
                                        <?php foreach ($attributes as $attribute) : ?>
                                            <p>
                                                <span class='card-text'><?= $attribute['attribute_name'] . ': ' . $attribute['value'] . $attribute['attribute_metric']; ?></span>
                                            </p>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                        ?>
        </div>
    </section>

    <footer class="footer">
        <p class="text-center">Scandiweb Test assignment</p>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="./assets/js/custom.js"></script>
    <script src="./assets/js/massDelete.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>