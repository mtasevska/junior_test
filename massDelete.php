<?php

namespace Product;

use DB;
use Product\Product;
use Product\ProductAttribute;

require_once './classes/Product.php';
require_once './classes/DB.php';
require_once './classes/ProductAttribute.php';

if ($_POST['ids']) {
    
    ProductAttribute::deleteAttributes($_POST['ids']);
    Product::massDelete($_POST['ids']);
}
