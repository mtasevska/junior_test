<!doctype html>
<html lang="en">

<head>
    <title>Add product</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <form action="productController.php" method="POST" id="product_form">
        <section id="header" class="mt-3">
            <div class="container">
                <div class="row d-flex">
                    <div class="col">
                        <h3>Product Add</h3>
                    </div>

                    <div class="col offset-5">
                        <button type="submit" class="btn-small btn-secondary">Save</button>
                        <button type="button" class="btn-small btn-danger"><a href="productList.php" class="text-decoration-none text-white">Cancel</a></button>
                    </div>
                </div>
                <hr class="mt-2 mb-3" />
            </div>
        </section>

        <section id="main">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="sku">SKU</label>
                            <input type="text" name="sku" id="sku" class="form-control" placeholder="Enter SKU" aria-describedby="helpId">
                            <label for="name" class="mt-2">Name</label>
                            <input type="text" id="name" name="name" id="name" class="form-control" placeholder="Enter Name" aria-describedby="helpId">
                            <label for="price" class="mt-2">Price ($)</label>
                            <input type="text" name="price" id="price" class="form-control" placeholder="Enter Price" aria-describedby="helpId">
                            <div class="form-group">
                                <label for="productType" class="mt-2">Type Switcher</label>
                                <select class="form-control" name="product_type_id" id="productType">
                                    <option selected>Type Switcher</option>
                                    <option value="1" id="DVD">DVD</option>
                                    <option value="2" id="Furniture">Furniture</option>
                                    <option value="3" id="Book">Book</option>
                                </select>
                            </div>

                            <div class="1 form">
                                <div class="form-group">
                                    <label for="size">Please, provide size:</label>
                                    <input type="text" name="size" id="size" class="form-control" placeholder="Size (MB)" aria-describedby="helpId">
                                </div>
                            </div>

                            <div class="2 form">
                                <div class="form-group">
                                    <label for="height"> Please, provide dimensions:</label>
                                    <input type="text" name="height" id="height" class="form-control" placeholder="Height (CM)" aria-describedby="helpId">
                                    <br>
                                    <input type="text" name="width" id="width" class="form-control" placeholder="Width (CM)" aria-describedby="helpId">
                                    <br>
                                    <input type="text" name="length" id="length" class="form-control" placeholder="Length (CM)" aria-describedby="helpId">
                                </div>
                            </div>

                            <div class="3 form">
                                <div class="form-group">
                                    <label for="weight">Please, provide weight:</label>
                                    <input type="text" name="weight" id="weight" class="form-control" placeholder="Weight (KG)" aria-describedby="helpId">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mt-5 mb-3" />
            </div>
        </section>
    </form>

    <footer>
        <p class="text-center">Scandiweb Test assignment</p>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script> 
    <script src="./assets/js/custom.js"></script>
    <script src="./assets/js/validation.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>