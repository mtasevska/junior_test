<?php
namespace Product;

include 'constants.php';

require_once './classes/DB.php';
require_once './classes/Product.php';

use Product\Product;
use Product\ProductTypes;

if($_SERVER["REQUEST_METHOD"] == "POST") {

$data = array_filter($_POST);

$product_type_id = $data['product_type_id'];
$product_type_name = ProductTypes::getTypeName($product_type_id);
$product_type = ucfirst(strtolower($product_type_name)); 

$classname = "Product\\" . $product_type;

$object = New $classname($data);

$attributes = [
    'size' => $data['size'],
    'height' => $data['height'],
    'width' => $data['width'],
    'length' => $data['length'],
    'weight' => $data['weight'],
];

$dataAttributes = array_filter($attributes);

Product::getAll();

Product::insert($object);

$classname::setAttributes($dataAttributes);

}

?>
