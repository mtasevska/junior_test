<?php

    use Product\Product;

    require_once './classes/Product.php';
?>

<!doctype html>
<html lang="en">

<head>
    <title>Product List</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css//style.css">
</head>

<body>
    <section id="header" class="mt-3">
        <div class="container">
            <div class="row d-flex">
                <div class="col">
                    <h3>Product List</h3>
                </div>

                <div class="col offset-5">
                    <button type="button" class="btn-small btn-secondary"><a href="productAdd.php" class="text-decoration-none text-white">ADD</a></button>
                    <button type="button" class="btn-small btn-danger" id="delete-product-btn">MASS DELETE</button>
                </div>
            </div>
            <hr class="mt-2 mb-3" />
        </div>
    </section>

    <section id="main">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card-deck">
                        <?php
                            $products = Product::getAll();
                            if (empty($products)) {
                                echo "<p class='text-center'>No products in database added</p>";
                            }
                            foreach ($products as $product) {
                                echo "
                                <div class='card' style='width: 18rem;'>
                                <div class='card-header'>
                                <div class='form-check'>
                                <input class='form-check-input delete-checkbox' type='checkbox' id='flexCheckDefault'>
                                </div>
                                </div>
                                
                                <div class='card-body text-center'>
                                <p class='card-text'>{$product['sku']}</p>
                                <p class='card-text'>{$product['name']}</p>
                                <p class='card-text'>{$product['price']}</p>
                                <p class='card-text'>Specific attribute value</p>
                                </div>
                                </div>";
                            }
                        ?>
                    </div>

                </div>
            </div>

            <hr class="mt-5 mb-3" />
        </div>
    </section>

    <footer>
        <p class="text-center">Scandiweb Test assignment</p>
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="./assets/js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>